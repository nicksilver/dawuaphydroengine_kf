import numpy as np


class KalmanFilter(object):
    def __init__(self, farm, ens_size):
        self.farm = farm
        self.ens_size = ens_size

    def _jitter_obs(self, obs, obs_params, param_names=['std'], dist_func=np.random.standard_normal):
        """
        Internal function to add noise to observations.

        :param obs: unpacked json observation data
        :param obs_params: unpacked json observation parameter data (i.e. mean and stddev.)
        :param param_names: list of names (str) for parameters. Do not include name of first parameter, defaults to the
            mean.
        :param dist_func: numpy distribution function
        :return: list of ens_size dicts randomly perturbed
        """

        ensembles = []
        for i in range(self.ens_size):
            ens_dict = {}
            for key, value in obs.iteritems():
                value = np.asarray(value)
                param_keys = [f + '_' + key for f in param_names]
                param = [np.asarray(obs_params.get(p)) for p in param_keys]
                ens_dict[key] = value + param[0]*dist_func()
            ensembles.append(ens_dict)
        return ensembles