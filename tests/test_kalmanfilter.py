from assimilation import kalmanfilter
import nose
import econengine as econ
import json
import numpy as np


class TestKalmanFilter(object):

    @classmethod
    def setup_class(self):
        print
        "SETUP!"

        self.ens_size = 1000

        with open('test_data/Farms.json') as json_farms:
            farms = json.load(json_farms)
        with open('test_data/observations.json') as json_obs:
            self.obs = json.load(json_obs)
        with open('test_data/obsparams.json') as json_params:
            self.obs_params = json.load(json_params)

        self.farm1 = farms['farms'][0]

        self.a = econ.econfuncs.Farm(**self.farm1)

        self.f = kalmanfilter.KalmanFilter(self.a, self.ens_size)

    @classmethod
    def teardown_class(cls):
        print
        "TEAR DOWN!"

    def setup_function(self):
        pass

    def teardown_function(self):
        pass

    def test_initialization(self):
        f = kalmanfilter.KalmanFilter(self.a, self.ens_size)
        nose.tools.assert_is_instance(f.farm, econ.Farm)

    def test_jitter_obs(self):
        #TODO Need to make this work for many parameters
        ens_jitter = self.f._jitter_obs(self.obs, self.obs_params)
        ens_mean = {}
        ens_std = {}
        # Find mean and stddev of the ensemble
        for key in self.obs:
            key_arr = np.asarray([ens_jitter[i][key] for i in range(self.ens_size)])
            key_mean = key_arr.mean(axis=0)
            key_std = key_arr.std(axis=0)
            ens_mean[key] = key_mean
            ens_std[key] = key_std
        # Compare mean and stddev of the ensemble with the observations
        for key in self.obs:
            mean_diff = ens_mean[key] - self.obs[key]
            std_diff = ens_std[key] - self.obs_params['std_' + key]
            mean_diff.mean()
            std_diff.mean()
        # nose.tools.assert_list_equal()

